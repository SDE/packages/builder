#!/bin/bash

__DIR__="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [[ -z "$(rpm --eval '%{?fedora}')" ]]; then
  echo "Error: Only supported on Fedora for now!"
  exit 1
fi

set -e

pkgdir=$(readlink -f ${__DIR__}/../packages)
scriptdir=${__DIR__}
repodir=$(readlink -f ${__DIR__}/..)

repobasename=fairsoft
gitbranch=$(git branch --show-current)
if [[ master == "${gitbranch}" ]]; then
  repoflavor=dev
else
  repoflavor=${gitbranch}
fi
reponame=${repobasename}-${repoflavor}

os=fedora
# os=centos
releasever=$(rpm --eval "%{fedora}")
# releasever=stream-8
arch=$(rpm --eval "%{_arch}")
releasedir=${os}-${releasever}-${arch}
if [[ ${os} = "fedora" ]] && [[ ${releasever} -lt 34 ]]; then
  mockroot=eol/${releasedir}
else
  mockroot=${releasedir}
fi
builddir=${repodir}/build/${releasedir}
resultdir=${repodir}/result/${releasedir}

mock="mock -r ${mockroot} --macro-file=${scriptdir}/macros"
rpmspec="rpmspec --load=${scriptdir}/macros"

mkdir -p ${builddir}
mkdir -p ${resultdir}

function status() {
  echo
  tree -h ${builddir} ${resultdir}
  echo
  du -hs ${builddir} ${resultdir}
}
